<?php

Class Model{
    public $pdo;

    function __construct() {
      $dsn = "mysql:
      host=".HOST.";
      dbname=".DB_NAME.";
      charset=".CHARSET.";
      ";
      $opt = array(
          PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
          PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
      );
      $this->pdo = new PDO($dsn, DB_USER, DB_PASS, $opt);
    }
}
?>
