<?php

Class View{

    function __construct($data) {
        $this->show_styles();
        $this->show_selector();
        $this->show_geo_tree($data['geo']['country_id']);

    }

    function show_geo_tree($data){
          print_r('<ul>');
          foreach ($data as $key => $value) {
              if(isset($value['name'])){
                   print_r('<li data-title="'.$value['description'].'">'.$value['name'].'</li>');
              }
              if(isset($value['elements'])){
                   $this->show_geo_tree($value['elements']);
              }elseif($key == 'region_id'){
                foreach ($value as $k => $v) {
                  if(isset($v['name'])){
                      print_r('<li data-title="'.$v['description'].'">'.$v['name'].'</li>');
                  }
                  if(isset($v['elements'])){
                       $this->show_geo_tree($v['elements']);
                  }
                }
              }
          }
         print_r('</ul>');
    }

    function show_styles(){
          echo '<style>
          li:hover::after {
            content: attr(data-title); /* Выводим текст */
            z-index: 1; /* Отображаем подсказку поверх других элементов */
            background: rgba(0,42,167,0.6); /* Полупрозрачный цвет фона */
            color: #fff; /* Цвет текста */
            text-align: center; /* Выравнивание текста по центру */
            font-family: Arial, sans-serif; /* Гарнитура шрифта */
            font-size: 11px; /* Размер текста подсказки */
            padding: 5px 10px; /* Поля */
            border: 1px solid #333; /* Параметры рамки */
           }
          </style>';
    }
    function show_selector(){
          $langs = array('rus','eng','ger');
          if(isset($_GET['user_lang']) && in_array($_GET['user_lang'], $langs ) ){
                $lang = $_GET['user_lang'];
          }
          echo '<select name="forma" onchange="location = this.value;">';
          foreach ($langs as $value) {
              if($value == $lang){
                  echo "<option selected='selected' value='/amt/index.php?user_lang=$value'>$value</option>";
              }
              else{
                  echo "<option value='/amt/index.php?user_lang=$value'>$value</option>";
              }
          }
          echo '</select>';
    }

}
?>
