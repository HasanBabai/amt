<?php

Class Controller{
    public $data;
    protected $results;

    function __construct($pdo) {
      $lang = DEFAULT_LANGUAGE;

      if(isset($_GET['user_lang']) && in_array($_GET['user_lang'], array('rus','eng','ger') ) ){
            $lang = $_GET['user_lang'];
      }

      $stmt = $pdo->query("SELECT
                          country.id AS country_id,
                          country.c_name_$lang AS country_name,
                          country.c_descr_$lang AS country_descr,
                          region.id AS region_id,
                          region.r_name_$lang AS region_name,
                          region.r_descr_$lang AS region_descr,
                          city.id AS city_id,
                          city.c_name_$lang AS name,
                          city.c_descr_$lang AS description
                          FROM country
                          INNER JOIN glob_region ON country.glob_region_id = glob_region.id
                          INNER JOIN city ON city.c_country_id = country.id
                          LEFT JOIN region ON region.id = city.c_region_id
                          WHERE glob_region.gr_name_eng LIKE  '%".GLOB_REGION."%'
                          ORDER BY country.id, region.id, city.id
                          ");
      $results = array();

      while ($row = $stmt->fetch())
      {
          $results[] = $row;
      }
      $group_by = array('country','region');
      $this->data['geo'] = $this->group_cities($results,$group_by);
    }

    function group_cities($arr,$group_by){
        $current_group = $group_by[0].'_id';

        $result = array($current_group => array());

        foreach($arr as $val){
            $id = $val[$current_group];
            if($id){
                $result[$current_group][$id][] = $val;
            }else{
                $val['single'] = true;
                $result[] = $val;
            }
        }

        ksort($result,SORT_NATURAL);

        foreach($result[$current_group] as $key => &$val){
          if(!isset($val['single']))
            $val = array(
                 'name' => $val[0][$group_by[0].'_name'],
                 'description' => $val[0][$group_by[0].'_descr'],
                 'elements' => $val
            );
        }

        if(count($group_by) > 1){
            array_shift($group_by);
            foreach($result[$current_group] as $key => &$val){
                $val['elements'] = $this->group_cities($val['elements'],$group_by);
            }
        }

        return $result;
    }
}

?>
